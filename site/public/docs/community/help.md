# Getting help

**If you have a general query about PubSweet**, or want to discuss anything with us, come and [chat to us in our Mattermost channel](https://mattermost.coko.foundation/coko/channels/pubsweet).

**Bug reports and feature requests** belong in the issues of the relevant repository:
  - [`pubsweet-server`](https://gitlab.coko.foundation/pubsweet/pubsweet-server/issues)
  - [`pubsweet-client`](https://gitlab.coko.foundation/pubsweet/pubsweet-client/issues)
  - [`pubsweet-cli`](https://gitlab.coko.foundation/pubsweet/pubsweet-cli/issues)

**If you're not sure where an issue belongs**, or to raise an issue about the PubSweet ecosystem in general, [use the pubsweet/pubsweet issues](https://gitlab.coko.foundation/pubsweet/pubsweet/issues).
